## Config server client
- http://localhost:9191/qcs-stats-service/actuator/health
- http://localhost:9191/qcs-stats-service/env
- http://localhost:9191/qcs-stats-service/actuator/refresh
    - call the above endpoint when you have update your properties file in the config-repo
- bootstrap.properties file defines the app name, the configure server location, and the active spring profile
- application.properties file defines the port that this client will be running on and also enable the 'actuator/refresh' endpoint
- StatsController.java has '/env' endpoint which pull the msg property from the config server
    - in the bootstrap.properties file, we define the active spring profile as (development) 'spring.profiles.active=development'
    - so, when we call '/env' endpoint, it will reach out to the config server and look for the property called 'env' in the 'qcs-development.properties' file of the config server
    - this is because we define the spring active profile as 'development' in the bootstrap.properties and because of this, it will read from the 'qcs-development.properties' because that file contains '-development'.
- when we update a property on any of the .properties file from config server, we just need to check that file in to git under the config server and we DO NOT need to restart the config server nor the config server client
- we DO need to call the POST '/actuator/refresh' endpoiont on the config server client so that it can pick up the changes from the config server

## Deploy to Heroku
- mvn clean heroku:deploy 
- https://qcs-stats-service.herokuapp.com/qcs-stats-service/actuator/health
- after app deploy and start, check gcd config server https://qcs-eureka-server.herokuapp.com/ 
and make sure it register qcs as a client
