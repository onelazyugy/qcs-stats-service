package com.qcs.statsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class QCSStatsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QCSStatsServiceApplication.class, args);
	}
}
