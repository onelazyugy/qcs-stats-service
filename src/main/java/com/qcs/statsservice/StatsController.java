package com.qcs.statsservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class StatsController {

    @Value("${env}")
    private String env;

    @RequestMapping(value = "/env", method = RequestMethod.GET)
    public String getEnv() {
        return this.env;
    }

}
